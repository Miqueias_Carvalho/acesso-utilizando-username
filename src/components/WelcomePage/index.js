import "./style.css";
function WelcomePage({ user, setIsLoggedIn }) {
  const HandleLogout = () => {
    setIsLoggedIn(false);
  };
  return (
    <div className="container_welcome">
      <h2>
        Bem-Vindo <span>{user}</span>!!!
      </h2>
      <button className="deslogar" onClick={HandleLogout}>
        Sair
      </button>
    </div>
  );
}
export default WelcomePage;
