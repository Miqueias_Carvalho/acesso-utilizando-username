import { useState } from "react";
import "./style.css";

function GetUserComponent({ setUser, setIsLoggedIn }) {
  const [userInput, setUserInput] = useState("");

  const HandleLogin = (userInput) => {
    setUser(userInput);
    setIsLoggedIn(true);
  };
  return (
    <form action="">
      <div className="container_input">
        <input
          type="text"
          placeholder="Digite seu Nome"
          value={userInput}
          onChange={(event) => setUserInput(event.target.value)}
        ></input>
      </div>
      <div className="container_botao">
        <button onClick={() => HandleLogin(userInput)}>Entrar</button>
      </div>
    </form>
  );
}
export default GetUserComponent;
